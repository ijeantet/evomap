## EvoMap

This repository contains the source code of the EvoMap framework developed jointly by the druid team at IRISA Rennes, France, and researchers from the Griffith University, Australia.

Hierarchy alignments can be done using the Jupyter Notebook _[hierarchy_alignment_tutorial.ipynb](https://gitlab.inria.fr/ijeantet/evomap/-/blob/master/hierarchy_alignment_tutorial.ipynb)_. While an EvoMap tutorial is available with the Jupyter Notebook _[EvoMap_tutorial.ipynb](https://gitlab.inria.fr/ijeantet/evomap/-/blob/master/EvoMap_tutorial.ipynb)_.

## Usage

from ./assets/EvoMap/lib.py:
* align_hierarchies
* evolutionary_map
* evo_map_similarity

## Packages

* **[OHC](https://gitlab.inria.fr/ijeantet/ohc)**:
Jeantet, I., Miklós, Z., & Gross-Amblard, D. (2020, April). Overlapping Hierarchical Clustering (OHC). In _International Symposium on Intelligent Data Analysis_ (pp. 261-273). Springer, Cham. https://doi.org/10.1007/978-3-030-44584-3_21

* **GAlign**:
Trung, H. T., Van Vinh, T., Tam, N. T., Yin, H., Weidlich, M., & Hung, N. Q. V. (2020, April). Adaptive Network Alignment with Unsupervised and Multi-order Convolutional Networks. In 2020 IEEE 36th International Conference on Data Engineering (ICDE) (pp. 85-96). IEEE.

* **[MUSE](https://github.com/facebookresearch/MUSE)**:
Conneau, A., Lample, G., Ranzato, M. A., Denoyer, L., & Jégou, H. (2017). Word translation without parallel data. arXiv preprint arXiv:1710.04087.

* **[VecMap](https://github.com/artetxem/vecmap)**:
Artetxe, M., Labaka, G., & Agirre, E. (2018, July). A robust self-learning method for fully unsupervised cross-lingual mappings of word embeddings. In _Proceedings of the 56th Annual Meeting of the Association for Computational Linguistics_ (Volume 1: Long Papers) (pp. 789-798).

* Many other more classical dependencies: **networkx**, **numpy**, **pandas**, **scipy**, **sklearn**, **torch**, ...

## Dataset

* **[Wiley](https://onlinelibrary.wiley.com/library-info/resources/text-and-datamining)** collection

## Author

* **Ian Jeantet** - *PhD student* - <ian.jeantet@irisa.fr>

## License

Copyright (C) 2018-2020 IRISA - Univ. Rennes 1

_This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version._

_This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details._

_You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>._

## Acknowledgments

* The ANR Program : [ANR-16-CE38-0002](http://www.agence-nationale-recherche.fr/Projet-ANR-16-CE38-0002)
* The Griffith University
* Our partners on the project:
  - [IHPST](http://ihpst.cnrs.fr/) - Institut d'Histoire et de Philosophie des Sciences et Techniques
  - [ISC-PIF](https://iscpif.fr/) -  Institut des Systèmes Complexes de Paris Ile de France
  - [LIP6](https://www.lip6.fr/) - Laboratoire Informatique de Paris 6
* Hat tip to anyone whose code was used
