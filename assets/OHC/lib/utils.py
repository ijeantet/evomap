"""
utils.py

Copyright (C) 2018-2020 Ian Jeantet <ian.jeantet@irisa.fr>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import os
# External
import pickle
import sortedcontainers
import sklearn
from ipywidgets import IntProgress, HTML, VBox

# General configuration
logger = logging.getLogger()


def log_config(level=logging.CRITICAL):
    """
    Sets the threshold for the logger 'script_name' to 'level'.
    Logging messages which are less severe than 'level' will be ignored.
    -----
    :param level:       Logger level (for instance: logging.INFO or logging.CRITICAL)
    :return:            Return the configured logger
    """
    logger = logging.getLogger()
    logging.basicConfig(format='%(asctime)s: (%(levelname)s) %(message)s')
    logger.setLevel(level)
    return logger


def save_log(file_name, data):
    """
    Save some data into a log file in the log folder
    -----
    :param file_name:   Name of the file where to store the metrics
    :param data:        Dictionary of the data to store. If the file does not exist, the keys are used as headers
    """
    # Get the log folder of the project
    log_path = '/'.join(os.path.dirname(os.path.abspath(__file__)).split('/')[:-1]) + "/log"
    # Create the log directory if necessary
    if not os.path.exists(log_path):
        os.makedirs(log_path)

    # Construct the path of the log file
    file_path = os.path.join(log_path, file_name)

    # If the file doesn't exist we need to add the csv header
    if not os.path.isfile(file_path):
        with open(file_path, "w+") as log_file:
            log_file.write(';'.join(data.keys()) + "\n")
    # Write the line in the file
    with open(file_path, "a+") as log_file:
        log_file.write(';'.join([str(d).replace('.', ',') for d in data.values()]) + "\n")

###################
# File processing #
###################


def get_param(file_name, param, param_sep='.', value_sep='_'):
    """
    Get the values of a parameter from the name of a file.
    -----
    For instance: from "../some/path/to/model.year_2004.ngrams_3.dbscan_clusters_003_5.txt"
                  if param = 'year' it will return ['2004'],
                  if param = 'dbscan_clusters' it will return ['003','5']
                  if param = 'ngram' it will return [] as there is a typo (should be 'ngrams'),
    :param file_name:   Name of the file where to extract the values of the param from.
    :param param:       Param whom the values are to extract.
    :param param_sep:   Optional, character that separates the parameters in the file name. '.' by default.
    :param value_sep:   Optional, character that separates the values of a parameter. '_' by default.
    :return:            The list of values as strings. An empty list if param was not found.
    """
    split_file_name = file_name.split('/')[-1].split(param_sep)
    for elem in split_file_name:
        if elem.startswith(param):
            if len(param) == len(elem):
                return []
            elif elem[len(param)] == value_sep:
                return elem.split(value_sep)[len(param.split(value_sep)):]
    return []


def update_param(file_name, param, new_value, param_sep='.', value_sep='_'):
    """
    Update the value of a parameter from the name of a file. If the param does not exist we simply add it.
    -----
    :param file_name:   Name of the file to update
    :param param:       Param to update
    :param new_value:   New value (or values) of the parameter
    :param param_sep:   Optional, character that separates the parameters in the file name. '.' by default
    :param value_sep:   Optional, character that separates the values of a parameter. '_' by default
    :return:            Return the modified name of the file
    """
    split_file_name = file_name.split('/')[-1].split(param_sep)
    # Merge the values if several are given
    if isinstance(new_value, list):
        new_value = value_sep.join([str(v) for v in new_value])
    else:
        new_value = str(new_value)
    # Detect the parameter to update
    found = False
    for i, elem in enumerate(split_file_name):
        if elem.startswith(param):
            split_file_name[i] = param + value_sep + new_value
            found = True
            break
    # If not found we add it
    if not found:
        split_file_name.insert(1, param + value_sep + new_value)
    return param_sep.join(split_file_name)


def data_crawl(data_path):
    """
    Crawl data_path and list the files that are located there.
    -----
    :param data_path:   A Path to a directory or to a single file
    :return:            The directory and a list of file names
    """
    # Files path management (if data_path is a single file or a folder)
    files = []
    dir_path = ''
    if os.path.isdir(data_path):
        files = os.listdir(data_path)
        dir_path = data_path
    elif os.path.isfile(data_path):
        files.append(data_path.split('/')[-1])
        dir_path = '/'.join(data_path.split('/')[:-1])
    return dir_path, files


def save_to_pickle(path, obj, name=None, wid_container=None):
    """
    Save an object as a pickle file. Take the name of the object if available.
    -----
    :param path:            Path where to save the hierarchy
    :param obj:             Object to save
    :param name:            (Optional) name of the file to create
    :param wid_container:   Container where to display the progresses and results. Default to None = standard output
    :return:        Return the name of the file
    """
    # Preconditions
    if not os.path.exists(path):
        os.makedirs(path)

    try:
        file_name = str(obj.name())
    except (AttributeError, TypeError):
        try:
            file_name = str(obj.name)
        except AttributeError:
            file_name = name

    if file_name is None:
        raise ValueError("Please give a file_name")

    file = os.path.join(path, file_name + ".pickle")
    with open(file, "wb") as f:
        pickle.dump(obj, f)
    saved = str(file.split('/')[-1]) + " saved"
    to_print(saved, wid_container)
    return file


def load_from_pickle(file_name):
    """
    Load an object from a pickle file.
    -----
    :param file_name:   Pickle file to load
    :return:            Return the loaded object
    """
    with open(file_name, "rb") as f:
        obj = pickle.load(f)
    logger.info(file_name.split('/')[-1] + " loaded")
    return obj

############################
# Vector related functions #
############################


def matrix_to_tuples(mat, triangle=False):
    """
    Transform a distance matrix into a list of tuples.
    -----
    :param mat:         Distance matrix
    :param triangle:    Indicate if we want to transform only the inferior triangle or the whole matrix into tuples
    :return:            Return a list of tuples (i, j, v) where i and j are the indices where the value v is in the
                        matrix
    """
    list_of_tuples = []
    for i in range(mat.shape[0]):
        for j in range(mat.shape[1]):
            if not triangle or i < j:
                list_of_tuples.append((i, j, mat[i][j]))
    return list_of_tuples


def tuple_distance(item):
    """
    Get the distance d of a tuple (a, b, d)
    -----
    :param item:    Tuple (a, b, d)
    :return:        Return d
    """
    return item[2]


def vectors_to_tuples(df, metric='cosine', wid_container=None):
    """
    Transform a dataframe of vectors to a sorted list of cosine distances (v1, v2, d(v1, v2))
    between any vectors v1 and v2 of df.
    -----
    :param df:              Dataframe with one vector on each line
    :param metric:          Metric used to compute the pairwise distances
    :param wid_container:   Container where to display the progresses and results. Default to None = standard output
    :return:    A sorted list of distances
    """
    nb_vectors = df.shape[0]
    pair_dist = str(int(nb_vectors*(nb_vectors-1)/2)) + " pairwise distances to compute (metric=" + metric + ")"
    to_print(pair_dist, wid_container)
    list_of_tuples = sortedcontainers.SortedList(key=lambda x: x[2])
    if nb_vectors > 1:
        for i in log_progress(range(1, nb_vectors), every=1, name="Processed vectors", wid_container=wid_container):
            vi = df.iloc[i].to_numpy()
            for j in range(i):
                vj = df.iloc[j].to_numpy()
                list_of_tuples.add((i, j, sklearn.metrics.pairwise_distances([vi], [vj], metric=metric)[0][0]))
    return list(list_of_tuples)


def barycenter(df_vectors):
    """
    Return the barycenter of the vector in the dataframe.
    -----
    :param df_vectors:  Dataframe containing some vectors (one per row)
    :return:            Return the barycenter of the vectors as a list of coordinates
    """
    return list(df_vectors.mean(0))


def cluster_to_hash(cluster, period_index=None):
    """
    Return a hash of the given cluster associated to its period if given.
    -----
    :param cluster:         Set of terms
    :param period_index:    (Optional) Index of the cluster period
    :return:                Return a hash of the given cluster associated to its period if given.
    """
    h = hash(frozenset(cluster))
    if period_index is not None:
        return str(h) + "_" + str(period_index)
    else:
        return str(h)

#######################
# visualization tools #
#######################


def to_print(string, wid_container=None):
    """
    Print string in a given container or on the standard output.
    -----
    :param string:          String value to print
    :param wid_container:   Container where to display the string. Default to None = standard output
    :return:
    """
    if wid_container:
        try:
            val = wid_container.value
            wid_container.value = val + "<br>" + string
        except AttributeError:
            wid_container_content = list(wid_container.children)
            try:
                val = wid_container_content[-1].value
                wid_container_content[-1].value = val + "<br>" + string
            except(IndexError, AttributeError):
                wid_container_content.append(HTML(value='<p style="line-height:1.6;">' + string))
                wid_container.children = wid_container_content
    else:
        print(string)


# Copyright Alexander Kukushkin https://github.com/kuk/log-progress
# Fork in PyPi: https://pypi.org/project/nbprogress/
def log_progress(sequence, every=None, size=None, name='Items', wid_container=None):

    is_iterator = False
    if size is None:
        try:
            size = len(sequence)
        except TypeError:
            is_iterator = True
    if size is not None:
        if every is None:
            if size <= 200:
                every = 1
            else:
                every = int(size / 200)     # every 0.5%
    else:
        assert every is not None, 'sequence is iterator, set every'

    if is_iterator:
        progress = IntProgress(min=0, max=1, value=1)
        progress.bar_style = 'info'
    else:
        progress = IntProgress(min=0, max=size, value=0)
    label = HTML()
    box = VBox(children=[label, progress])
    if wid_container:
        wid_container_content = list(wid_container.children)
        wid_container_content.append(box)
        wid_container.children = wid_container_content
    # else:
    #    display(box)

    index = 0
    try:
        for index, record in enumerate(sequence, 1):
            if index == 1 or index % every == 0:
                if is_iterator:
                    label.value = '{name}: {index} / ?'.format(
                        name=name,
                        index=index
                    )
                else:
                    progress.value = index
                    label.value = u'{name}: {index} / {size}'.format(
                        name=name,
                        index=index,
                        size=size
                    )
            yield record
    except:
        progress.value = index
        label.value = u'{name}: {index} / {size}'.format(
            name=name,
            index=index,
            size=size
        )
        progress.bar_style = 'danger'
        raise
    else:
        progress.bar_style = 'success'
        progress.value = index
        label.value = "{name}: {index}".format(
            name=name,
            index=str(index or '?')
        )
