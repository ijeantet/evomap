"""
embedding.py:

Copyright (C) 2019-2020 Ian Jeantet <ian.jeantet@irisa.fr>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import multiprocessing
import os
# External
import gensim
import pandas as pd
# Local files
import lib.utils as utils

# General configuration
logger = logging.getLogger()


def generate_embeddings(data, models_path, opt=4, period_size=1, period=None, step=1, stop_word_file=None,
                        embedding_size=300, learning_rate=0.025, window_size=2, min_count=10, subsample=1e-3,
                        workers=multiprocessing.cpu_count(), sg=True, num_samples=5, epochs_to_train=5, batch_size=100,
                        max_final_vocab=None, fixed_vocab=None):
    """
    Deternime a vocabulary and generate the corresponding embeddings using gensim SGNS technique.
    -----
    For the gensim word2vec parameters, please look at the doc here: see the doc here:
    https://radimrehurek.com/gensim/models/word2vec.html#gensim.models.word2vec.Word2Vec
    :param data:            Folder containing the files to process
    :param models_path:     Path where to store the generated models
    :param opt:             Vocabulary option in {1:previous, 2:union, 3:intersection, 4:new, 5:fixed}
    :param period_size:     Size of the time window over the data (in terms of number of files for one period)
    :param period:          Period parameter to build correct file names ("year", "publication_year", "month"...)
    :param step:            Indicate how to slide the time window over the files
    :param stop_word_file:  File containing a set of stop words to ignore
    :param embedding_size:  Size of the embeddings
    :param learning_rate:   Gensim word2vec parameter
    :param window_size:     Gensim word2vec parameter
    :param min_count:       Gensim word2vec parameter
    :param subsample:       Gensim word2vec parameter
    :param workers:         Gensim word2vec parameter
    :param sg:              Gensim word2vec parameter
    :param num_samples:     Gensim word2vec parameter
    :param epochs_to_train: Gensim word2vec parameter
    :param batch_size:      Gensim word2vec parameter
    :param max_final_vocab  Gensim word2vec parameter
    :param fixed_vocab      List of key words to use as vocab
    :return:                None
    """

    nb_processed_files = 0
    nb_models = 0
    nb_empty_files = 0
    nb_empty_vocabs = 0
    previous_model = None
    previous_model_file = ""

    # Processing
    if not 0 < opt <= 5:
        print('Please chose a correct vocabulary option in {1:previous, 2:union, 3:intersection, 4:new, 5:fixed}')
    else:

        if fixed_vocab is None:
            fixed_vocab = dict()

        # Files path management (if data_path is a single file or a folder)
        files = []
        dir_path = ''
        if isinstance(data, list):
            files = data
        elif os.path.isdir(data):
            files = os.listdir(data)
            dir_path = data
        elif os.path.isfile(data):
            files.append(data.split('/')[-1])
            dir_path = '/'.join(data.split('/')[:-1])

        # Saving path management
        if not os.path.exists(models_path):
            os.makedirs(models_path)

        # Stop words management
        stop_list = set()
        if stop_word_file:
            with open(stop_word_file, 'r') as stop_words:
                stop_list = set(stop_words.read().split('\n'))

        files = sorted(files)
        nb_files = len(files)
        print(str(len(files)) + " files found")

        start = 0
        sentences_tab = []
        while start + period_size <= nb_files:
            for file_name in files[start+len(sentences_tab):start+period_size]:
                if file_name.endswith(".txt"):
                    nb_processed_files += 1
                    # Checking if the file is empty or not
                    if os.stat(os.path.join(dir_path, file_name)).st_size == 0:
                        logger.info(file_name + " was ignored: empty file")
                        nb_empty_files += 1
                    else:
                        print("Processing " + file_name)

                        # Format the data of the file
                        sentences_tmp = []
                        for line in open(os.path.join(dir_path, file_name), 'r'):
                            sentences_tmp.append([w for w in line.strip().split(" ") if
                                                  w not in stop_list])  # Remove the stop-words from stop_list
                        sentences_tab.append(sentences_tmp)

            # Merge the data of the periods of the window
            sentences = []
            for s in sentences_tab:
                sentences += s

            # Build an empty model with the correct hyperparameters
            model = gensim.models.Word2Vec(size=embedding_size, alpha=learning_rate, window=window_size,
                                           min_count=min_count, sample=subsample, workers=workers, sg=sg,
                                           negative=num_samples, iter=epochs_to_train, batch_words=batch_size,
                                           max_final_vocab=max_final_vocab)

            # First model vocab computation
            if not previous_model and opt <= 4:
                model.build_vocab(sentences)

            # Other model vocab computation
            else:
                # Vocabulary option
                if opt == 1:
                    # 1 Replacement of the vocab
                    # We need to convert the words of the previous model to a dict before passing
                    # them to the function
                    model.build_vocab_from_freq({str(word): vocab_obj.count for word, vocab_obj in
                                                 previous_model.vocab.items()}, update=False)
                elif opt == 2:
                    # 2 Union of the dictionaries
                    model.build_vocab(sentences)
                    # Add the new words from the previous model to the vocab to the current model
                    # (we need to convert them as a dict to use the function bellow)
                    if model.wv.vocab:
                        model.build_vocab_from_freq(
                            {str(word): vocab_obj.count for word, vocab_obj in
                             previous_model.vocab.items()}, update=True)
                    else:
                        model.build_vocab_from_freq(
                            {str(word): vocab_obj.count for word, vocab_obj in
                             previous_model.vocab.items()}, update=False)
                elif opt == 3:
                    # 3 Intersection of the dictionaries
                    model_tmp = gensim.models.Word2Vec(size=embedding_size, alpha=learning_rate,
                                                       window=window_size, min_count=min_count,
                                                       sample=subsample, workers=workers, sg=sg,
                                                       negative=num_samples, iter=epochs_to_train,
                                                       batch_words=batch_size, max_final_vocab=max_final_vocab)
                    model_tmp.build_vocab(sentences)
                    vocab_tmp = set(model_tmp.wv.vocab) & set(previous_model.vocab)
                    if not vocab_tmp:  # If no word in common we use the vocab of the new model
                        vocab_tmp = set(model_tmp.wv.vocab)
                    model.build_vocab_from_freq(
                        {str(word): vocab_obj.count for word, vocab_obj in model_tmp.wv.vocab.items()
                         if word in vocab_tmp}, update=False)
                elif opt == 5:
                    # 5 intersection of the vocab with the fixed one given in parameter
                    model_tmp = gensim.models.Word2Vec(size=embedding_size, alpha=learning_rate,
                                                       window=window_size, min_count=5,
                                                       sample=subsample, workers=workers, sg=sg,
                                                       negative=num_samples, iter=epochs_to_train,
                                                       batch_words=batch_size, max_final_vocab=None)
                    model_tmp.build_vocab(sentences)
                    # print(len(model_tmp.wv.vocab))
                    vocab_tmp = set(model_tmp.wv.vocab) & set(fixed_vocab)
                    # print(vocab_tmp)
                    if not vocab_tmp:  # If no word in common we use the vocab of the new model
                        vocab_tmp = set(model_tmp.wv.vocab)
                    model.build_vocab_from_freq(
                        {str(word): vocab_obj.count for word, vocab_obj in model_tmp.wv.vocab.items()
                         if word in vocab_tmp}, update=False)
                else:  # opt == 4
                    # 4 Only vocabulary from the 2nd file
                    model.build_vocab(sentences)
                logger.info(str(len(model.wv.vocab)) + " words in the vocabulary of the 2nd model")

            model_name = files[start+period_size-1]
            if period:
                period_start = utils.get_param(files[start], period)
                period_stop = utils.get_param(files[start+period_size-1], period)
                if period_start != period_stop:
                    model_name = utils.update_param(files[start], period, period_start + period_stop)

            # Ignore if the vocab is empty
            if not model.wv.vocab:
                nb_empty_vocabs += 1
                logger.info(model_name + " was ignored: empty vocab")

            # else update the word vectors, train the model and save it
            else:
                if previous_model_file:
                    # Updating positions of the vectors of the words present in the model1
                    model.intersect_word2vec_format(previous_model_file, binary=False,
                                                    lockf=1.0)

                # Training
                training_examples_count = len(sentences)
                logger.info(str(training_examples_count) + " data will be used for the training")
                model.train(sentences, total_examples=training_examples_count, epochs=model.epochs)
                logger.info("Model trained")

                # Save the word vectors of the model
                # Save also gensim format model as 'previous_model_file' to update the words vectors
                # of the following model
                save_model_to_csv(model_name, models_path, model)
                nb_models += 1
                if previous_model_file:
                    os.remove(previous_model_file)
                previous_model_file = save_model(model_name, "/tmp/", model, binary_model=False)

                previous_model = model.wv
                logger.info("Model saved")

            if step < len(sentences_tab):
                sentences_tab = sentences_tab[step:]
            else:
                sentences_tab = []
            start += step

        print(str(nb_processed_files) + " files with the good criteria processed (including " + str(nb_empty_files)
              + " empty files and " + str(nb_empty_vocabs) + " empty vocabs)")
        print(str(nb_models) + " models generated")


def save_model(file_name, save_path, model, binary_model=True):
    """Saves the word vectors of the model"""
    # Preconditions
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    # Processing
    output_file_prefix = os.path.join(save_path, '.'.join(file_name.split('/')[-1].split('.')[:-1]))

    if binary_model:  # Save a binary format model
        output_file_name = output_file_prefix + ".word2vec.bin"
    else:  # Save a text format model
        output_file_name = output_file_prefix + ".word2vec.wv"

    model.wv.save_word2vec_format(output_file_name, binary=binary_model)

    output_vocab_name = output_file_prefix + ".vocab.txt"
    with open(output_vocab_name, 'w') as vocab_file:
        for word, vocab_obj in model.wv.vocab.items():
            vocab_file.write(word + "," + str(vocab_obj.count) + "\n")

    return output_file_name


def save_model_to_csv(file_name, save_path, model):
    """Writes a csv file containing the words, their word vectors and their frequency"""
    # Preconditions
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    # Processing
    output_file_prefix = os.path.join(save_path, '.'.join(file_name.split('/')[-1].split('.')[:-1]))
    output_file_name = output_file_prefix + ".word2vec.csv"

    with open(output_file_name, 'w') as csv_file:

        # Write headers
        fieldnames = ['Word']
        for i in range(len(model.wv[list(model.wv.vocab)[0]])):
            fieldnames.append(str(i))
        fieldnames.append('Frequency')
        csv_file.write(",".join(fieldnames) + "\n")

        # Write data
        for word, vocab_obj in model.wv.vocab.items():
            vector = model.wv[word]
            # str_vector = ",".join(list(vector))
            csv_file.write(word + "," + str(list(vector)).strip('[]') + "," + str(vocab_obj.count) + "\n")
    nb_word_vectors = len(model.wv.vocab.items())
    print(output_file_name + " saved (" + str(nb_word_vectors) + " word vectors generated)")
    return output_file_name


def load_model_from_csv(file_name, sort=True):
    """
    Load a model from a csv file into a dataframe
    -----
    :param file_name:   Model file to load
    :param sort:        Optional, if true the dataframe is sorted according to the word frequency
    :return:            Return the dataframe
    """
    df = pd.read_csv(file_name, sep=',', header=0, converters={'Word': str})  # index_col=0,
    df = df.set_index('Word')
    if sort:
        df = df.sort_values(by=['Frequency'], ascending=False)
    return df
