"""
lib.py:

Copyright (C) 2019-2020 Ian Jeantet <ian.jeantet@irisa.fr>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import os
import pickle
import subprocess
import pandas as pd
import networkx as nx
import torch
import numpy as np
import graphviz
import math
import ipywidgets as widgets
from sklearn.preprocessing import normalize
import shutil
import random

from assets.OHC.lib.utils import barycenter, save_to_pickle, load_from_pickle, to_print, log_progress, get_param, cluster_to_hash
from assets.OHC.lib.embedding import load_model_from_csv
from assets.OHC.lib.metric import jaccard_similarity
from assets.FAGCN import FAGCN
from assets.VecMap.embeddings import read


def Laplacian_graph(A):
    """
    Normalized Laplacian matrix calculator.
    -----
    :param A:   Matrix to normalize
    :return:    The normalized matrix
    """
    for i in range(len(A)):
        A[i, i] = 1
    A = torch.FloatTensor(A)
    D_ = torch.diag(torch.sum(A, 0) ** (-0.5))
    A_hat = torch.matmul(torch.matmul(D_, A), D_)
    A_hat = A_hat.float()
    indices = torch.nonzero(A_hat).t()
    values = A_hat[indices[0], indices[1]]
    A_hat = torch.sparse.FloatTensor(indices, values, A_hat.size())
    return A_hat


def align_hierarchies(hierarchies, models, epochs=1000, save_path="", embedding_alignment="vecmap", reuse=True,
                      period=None, top_k=-1, wid_container=None):
    """
    Perform an alignment of the hierarchies using the G_align algorithm
    -----
    :param hierarchies:         List of hierarchies to align
    :param models:              List of embedding models associated to the hierarchies
    :param epochs:              Number of epochs for the GAlign learning algorithm
    :param save_path:           Path where to save the alignments
    :param embedding_alignment: Embedding alignment to use (either "vecmap" or "muse")
    :param reuse:               If true it will look in save_path for existing alignments
    :param period:              Name of the periods of time ("date", "year",...)
    :param top_k:               Number of upper levels of the hierarchies to align ("-1" for the complete hierarchies)
    :param wid_container:       (Optional) Widget container where to display the process information.
                                If None the standard output is used
    :return:                    Return a list of alignment objects (loaded or computed)
    """

    # Constants #
    #############

    # Temporary emb.txt file
    exp_path = "./"
    exp_name = "tmp"
    if not os.path.exists(os.path.join(exp_path, exp_name)):
        os.makedirs(os.path.join(exp_path, exp_name))
    exp_id = "hierarchy_alignment"
    # Input files
    source_emb_txt = os.path.join(exp_path, exp_name, "source.emb.txt")
    target_emb_txt = os.path.join(exp_path, exp_name, "target.emb.txt")
    train_dict = os.path.join(exp_path, exp_name, "train_dict.txt")
    # Output files
    src_mapped_emb = os.path.join(exp_path, exp_name, "source_mapped.emb.txt")
    trg_mapped_emb = os.path.join(exp_path, exp_name, "target_mapped.emb.txt")

    nb_hierarchies = len(hierarchies)

    # Variables #
    #############

    i1 = 0
    i2 = 1
    hierarchy_source = None
    df_source_model = None
    alignment_loaded = True

    # Output values
    alignments = []
    accuracies = []
    alignment_labels = []

    # Init #
    ########

#    if nb_hierarchies > 0:
#        # Load the first hierarchy
#        with open(hierarchies[0], "rb") as openfile:
#            hierarchy_source = pickle.load(openfile)
#
#        # Load the first model
#        df_source_model = load_model_from_csv(models[i1], sort=False)
#        df_source_model = df_source_model.iloc[:, 0:-1]  # Remove the frequency column
#
#        # Generate first VecMap input files
#        with open(source_emb_txt, 'w') as output_file:
#            output_file.write(str(df_source_model.shape[0]) + " " + str(df_source_model.shape[1]) + "\n")
#            df_source_model.to_csv(output_file, sep=' ', index=True, header=False)
#            # print(source_emb_txt + " saved")

    # Init widgets #
    ################

    if wid_container:
        wid_tab_content = []
        wid_tab = widgets.Tab(children=wid_tab_content)  # One tab per alignment
        wid_nb_alignments = widgets.Button(description="0")
        wid_counters = widgets.HBox([widgets.Button(description="Alignments"), wid_nb_alignments])
        wid_container.children = [wid_tab, wid_counters]

    # Alignment #
    ############

    while i2 < nb_hierarchies:

        # Init a new widget for the current alignment #
        ###############################################

        if wid_container:
            wid_alignment_content = []
            wid_alignment = widgets.VBox(children=wid_alignment_content)  # For the current hierarchy
            wid_tab_content.append(wid_alignment)
            wid_tab.children = wid_tab_content
            wid_tab.set_title(str(i2-1), str(i2))
        else:
            wid_alignment = None
            print("\n")
            print("--------------- Alignment " + str(i2) + " ---------------")

        # Precomputation verification #
        ###############################

        h1_name = '.'.join(hierarchies[i1].split('/')[-1].split('.')[:-2])
        h2_name = '.'.join(hierarchies[i2].split('/')[-1].split('.')[:-2])
        if wid_container:
            to_print("From <i>" + h1_name + "</i>", wid_alignment)
            to_print("to <i>" + h2_name + "</i>", wid_alignment)
        else:
            print("From " + h1_name)
            print("to " + h2_name)

        alignment_name = h1_name + "." + h2_name + ".emb_align_" + embedding_alignment.lower()
        if top_k > 0:
            alignment_name += ".top_levels_" + str(top_k)
        alignment_name += ".alignment"
        alignment_file = os.path.join(save_path, alignment_name + ".pickle")
        if save_path and reuse and os.path.isfile(alignment_file):
            # Load the alignment instead of recomputing it
            alignment = load_from_pickle(alignment_file)
            # aligned_dict = alignment["aligned_dict"]
            accuracy = alignment["emb_alignment_acc"]
            alignment_label = alignment["period_source"] + " -> " + alignment["period_target"]
            if wid_container:
                to_print("Alignment found and loaded (<i>" + alignment_name + ".pickle</i>)", wid_alignment)
            else:
                print("Alignment found and loaded (" + alignment_name + ".pickle)")

            alignment_loaded = True
        else:

            # Compute the alignment label
            h1_period = str(i1)
            h2_period = str(i2)
            if period:
                period_values_h1 = get_param(hierarchies[i1], period)
                if period_values_h1:
                    h1_period = "-".join([str(pv) for pv in period_values_h1])
                period_values_h2 = get_param(hierarchies[i2], period)
                if period_values_h2:
                    h2_period = "-".join([str(pv) for pv in period_values_h2])
            alignment_label = h1_period + " -> " + h2_period

            # Data loading (source and target) #
            ####################################

            to_print("--------------- Data loading ---------------", wid_alignment)

            # Load the source hierarchy in the case we couldn't take the previous target as the new source
            if alignment_loaded:
                with open(hierarchies[i1], "rb") as openfile:
                    hierarchy_source = pickle.load(openfile)
                # Reduce the graphs to their top k levels
                if top_k > 0:
                    hierarchy_source = top_levels(hierarchy_source, k=top_k)
            # Load the target hierarchy
            with open(hierarchies[i2], "rb") as openfile:
                hierarchy_target = pickle.load(openfile)
            # Reduce the graphs to their top k levels
            if top_k > 0:
                hierarchy_target = top_levels(hierarchy_target, k=top_k)
                to_print("Cutting graphs to " + str(top_k) + " levels:", wid_alignment)

            to_print("Source: " + str(len(hierarchy_source.nodes)) + " nodes and " + str(len(hierarchy_source.edges))
                     + " edges", wid_alignment)
            to_print("Target: " + str(len(hierarchy_target.nodes)) + " nodes and " + str(len(hierarchy_target.edges))
                     + " edges", wid_alignment)

            # Load the source model in the case we couldn't take the previous target as the new source
            if alignment_loaded:
                df_source_model = load_model_from_csv(models[i1], sort=False)
                df_source_model = df_source_model.iloc[:, 0:-1]  # Remove the frequency column
            # Load target model
            df_target_model = load_model_from_csv(models[i2], sort=False)
            df_target_model = df_target_model.iloc[:, 0:-1]  # Remove the frequency column

            if wid_container:
                to_print("'<i>" + models[i1].split('/')[-1] + "</i>' loaded -> " + str(df_source_model.shape[0])
                         + " word vectors", wid_alignment)
                to_print("'<i>" + models[i2].split('/')[-1] + "</i>' loaded -> " + str(df_target_model.shape[0])
                         + " word vectors", wid_alignment)
            else:
                print(models[i1].split('/')[-1] + " loaded -> " + str(df_source_model.shape[0]) + " word vectors")
                print(models[i2].split('/')[-1] + " loaded -> " + str(df_target_model.shape[0]) + " word vectors")

            # Embedding space alignment #
            #############################

            to_print("--------------- Embedding space alignment ---------------", wid_alignment)

            # Generate VecMap/MUSE input files
            # The source file was already saved during the previous alignment except if it was loaded and not computed
            if alignment_loaded:
                with open(source_emb_txt, 'w') as output_file:
                    output_file.write(str(df_source_model.shape[0]) + " " + str(df_source_model.shape[1]) + "\n")
                    df_source_model.to_csv(output_file, sep=' ', index=True, header=False)
                    to_print(source_emb_txt + " saved", wid_alignment)
            # target file
            with open(target_emb_txt, 'w') as output_file:
                output_file.write(str(df_target_model.shape[0]) + " " + str(df_target_model.shape[1]) + "\n")
                df_target_model.to_csv(output_file, sep=' ', index=True, header=False)
                to_print(target_emb_txt + " saved", wid_alignment)

            # Train.dict
            vocab1 = set(df_source_model.index)
            vocab2 = set(df_target_model.index)

            voc_inter = len(vocab1.intersection(vocab2))
            voc_union = len(vocab1.union(vocab2))
            common = round(100 * voc_inter / voc_union, 2)
            if wid_container:
                to_print(str(voc_inter) + "/" + str(voc_union) + " identical words (<b>%.2f" % common + "%</b>)",
                         wid_alignment)
            else:
                print(str(voc_inter) + "/" + str(voc_union) + " identical words (%.2f" % common + "%)")

            with open(train_dict, 'w') as output_file:
                for w in vocab1.intersection(vocab2):
                    output_file.write(w + " " + w + "\n")
                to_print(train_dict + " saved", wid_alignment)

            # VecMap #
            if embedding_alignment.lower() == "vecmap":

                to_print("Using VecMap", wid_alignment)

                # TODO make the subprocess work with a real relative path to map_embeddings.py
                mapping = subprocess.check_output(['python3', './assets/VecMap/map_embeddings.py', '--supervised',
                                                   train_dict, source_emb_txt, target_emb_txt, src_mapped_emb,
                                                   trg_mapped_emb])
                if mapping:
                    to_print(mapping.decode("utf-8"), wid_alignment)

            # MUSE #
            elif embedding_alignment.lower() == "muse":

                to_print("Using MUSE", wid_alignment)

                mapping = subprocess.check_output(['python3', './assets/MUSE/supervised.py', '--cuda', 'False',
                                                   '--src_lang', 'source_mapped.emb',
                                                   '--tgt_lang', 'target_mapped.emb',
                                                   '--src_emb', source_emb_txt,
                                                   '--tgt_emb', target_emb_txt,
                                                   '--exp_path', exp_path,
                                                   '--exp_name', exp_name,
                                                   '--exp_id', exp_id,
                                                   '--n_refinement', '5',
                                                   '--dico_train', train_dict,
                                                   '--dico_eval', train_dict
                                                   ])
                if mapping:
                    to_print(mapping.decode("utf-8"), wid_alignment)

                # Move and renames the results to match the output files the the rest of the process
                shutil.move(os.path.join(exp_path, exp_name, exp_id, "vectors-source_mapped.emb.txt"), src_mapped_emb)
                shutil.move(os.path.join(exp_path, exp_name, exp_id, "vectors-target_mapped.emb.txt"), trg_mapped_emb)

                # Remove other output files
                shutil.rmtree(os.path.join(exp_path, exp_name, exp_id))

            # Embedding alignment evaluation #
            coverage = subprocess.check_output(['python3', './assets/VecMap/eval_translation.py',
                                                src_mapped_emb, trg_mapped_emb, '-d', train_dict])\
                .decode("utf-8")
            accuracy = float(coverage.split(' ')[-1].split(' ')[-1].split(':')[-1][:-2])
            if wid_container:
                to_print("<b>" + coverage + "</b>", wid_alignment)
            else:
                print(coverage)

            # Vector normalization #
            ########################

            to_print("--------------- Vector normalization ---------------", wid_alignment)

            # Read input embeddings
            srcfile = open(src_mapped_emb, errors='surrogateescape')
            src_words, x = read(srcfile)
            df_source_mapped_emb = pd.DataFrame(x, index=src_words)

            trgfile = open(trg_mapped_emb, errors='surrogateescape')
            trg_words, z = read(trgfile)
            df_target_mapped_emb = pd.DataFrame(z, index=trg_words)

            df_source_norm = pd.DataFrame(normalize(df_source_mapped_emb), index=df_source_mapped_emb.index)
            to_print(str(len(src_words)) + " source vectors normalized", wid_alignment)
            df_target_norm = pd.DataFrame(normalize(df_target_mapped_emb), index=df_target_mapped_emb.index)
            to_print(str(len(trg_words)) + " target vectors normalized", wid_alignment)

            # Feature computation #
            #######################

            to_print("--------------- Feature computation ---------------", wid_alignment)

            # Get the hierarchy nodes
            nodes_info_source = pd.DataFrame(list(dict(hierarchy_source.nodes(data=True)).values()))
            nodes_info_target = pd.DataFrame(list(dict(hierarchy_target.nodes(data=True)).values()))

            # Compute the features
            features_s = [barycenter(df_source_norm.loc[list(c)]) for c in nodes_info_source["cluster"]]
            to_print("Source hierarchy: done", wid_alignment)
            features_t = [barycenter(df_target_norm.loc[list(c)]) for c in nodes_info_target["cluster"]]
            to_print("Target hierarchy: done", wid_alignment)

            to_print("Feature dimension: " + str(len(features_s[0])), wid_alignment)  # Feature_dim 300

            # Graph preprocessing #
            #######################

            to_print("--------------- Graph preprocessing ---------------", wid_alignment)

            # Compute the adj matrices
            adj_s = nx.adjacency_matrix(hierarchy_source).todense()
            adj_t = nx.adjacency_matrix(hierarchy_target).todense()
            adj_s[adj_s > 0] = 1
            adj_t[adj_t > 0] = 1
            adj_s += adj_s.T
            adj_t += adj_t.T

            # Normalized Laplacian matrix computation
            source_A_hat = Laplacian_graph(adj_s)
            target_A_hat = Laplacian_graph(adj_t)

            to_print("Done", wid_alignment)

            # Hierarchy alignment #
            #######################

            wid_alignment_acc_content = None
            if wid_container:
                wid_alignment_acc_content = widgets.VBox(layout=widgets.Layout(overflow='scroll', height='80px'))
                wid_alignment_acc = widgets.Accordion(children=[wid_alignment_acc_content], selected_index = None)
                wid_alignment_acc.set_title(0, "--------------- Hierarchy alignment ---------------")
                wid_alignment_content = list(wid_alignment.children)
                wid_alignment_content.append(wid_alignment_acc)
                wid_alignment.children = wid_alignment_content
            else:
                print("--------------- Hierarchy alignment ---------------")

            # NOTE: I had to set self.cuda = False in the lib file -> add a paramter?
            align_model = FAGCN.FAGCN(np.array(features_s), np.array(features_t), source_A_hat, target_A_hat,
                                      num_epochs=epochs, learning_rate=0.001)
            # NOTE: I redirected the FAGCN output as it's crashing my notebook (too many lines to display)
            wid_dummy = widgets.VBox()
            similarity_matrix = align_model.align(wid_container=wid_dummy)
            # print(similarity_matrix.shape())

            # Create diction of aligned pairs
            alignment = {"hierarchy_source": h1_name,
                         "period_source": h1_period,
                         "hierarchy_target": h2_name,
                         "period_target": h2_period,
                         "emb_alignment_alg": embedding_alignment,
                         "emb_alignment_voc": (voc_inter, voc_union),
                         "emb_alignment_acc": accuracy,
                         "top_k": top_k,
                         "id2clusters_s": [set(cs) for cs in nodes_info_source["cluster"]],
                         "id2clusters_t": [set(ct) for ct in nodes_info_target["cluster"]],
                         "df_alignment": pd.DataFrame(similarity_matrix)}

            if save_path:
                save_to_pickle(save_path, alignment, name=alignment_name, wid_container=wid_alignment)

            alignment_loaded = False

        # Add the alignment dictionary to the list of alignments
        alignments.append(alignment)
        accuracies.append(accuracy)
        alignment_labels.append(alignment_label)
        if wid_container:
            wid_nb_alignments.description = str(len(alignments))

        # Avoid some computation by moving the targets that will become the new sources
        if not alignment_loaded:
            hierarchy_source = hierarchy_target
            df_source_model = df_target_model
            os.rename(target_emb_txt, source_emb_txt)

        i1 += 1
        i2 += 1

    return alignments, accuracies, alignment_labels


def top_levels(ohc_graph, k):
    """
    Return a subgraph containing only nodes from the top k levels of the hierarchy
    -----
    :param ohc_graph:   The hierarchy as a networkx graph
    :param k:           Number of wanted levels
    :return:            Return the subgraph as a networkx graph
    """
    level_thresholds = ohc_graph.graph["level_thresholds"]
    if k >= len(level_thresholds):
        return ohc_graph
    else:
        d_min = level_thresholds[-k]
        nodes_to_remove = [n[0] for n in ohc_graph.nodes(data=True) if n[1]["d_death"] <= d_min]
        ohc_copy = ohc_graph.copy()
        ohc_copy.remove_nodes_from(nodes_to_remove)
    return ohc_copy

####################
# EvoMap functions #
####################


def detect_cluster(graph, cluster):
    """
    Look for the node that contains the most number of words of the cluster given in parameter,
    walking down the hierarchy from its root
    -----
    :param graph:   The hierarchy as a networkx graph with specific nodes
    :param cluster: Bag of words
    :return:        Return the best match node found in the graph
    """
    # Extract node attributes
    nodes_info = pd.DataFrame(list(dict(graph.nodes(data=True)).values()), index=graph.nodes())
    nodes_info = nodes_info.sort_values(by=['d_birth'], ascending=True)
    root = graph.nodes[nodes_info.index[-1]]

    # Adapt the bag of word to the vocab of the graph
    adapted_clust = set(cluster).intersection(root["cluster"])
    n = len(adapted_clust)
    # If no common word found
    if n == 0:
        return -1, dict()

    node_index = nodes_info.index[-1]
    best_candidate = root
    if len(root["cluster"]) == n:
        return node_index, best_candidate
    else:
        while 1:
            node_tmp = None
            children = list(graph.predecessors(node_index))
            for child_index in children:
                if adapted_clust.issubset(graph.nodes[child_index]["cluster"]):
                    node_index = child_index
                    node_tmp = graph.nodes[child_index]
                    break
            if not node_tmp:
                return node_index, best_candidate
            else:
                best_candidate = node_tmp


def future_evolution(node, hierarchies, alignments, stop=True):
    """
    Use the cluster_index in parameter to follows its evolution through the hierarchies by using the alignments.
    -----
    :param node:            Node of hierarchies[0]
    :param hierarchies:     List of n hierarchy files
    :param alignments:      List of n-1 associating dictionaries
    :param stop:            (Optional) If true we stop the evolution when no elements are shared by consecutive nodes
    :return:                Return a list of nodes containing the evolution of the cluster with one node per period
    """

    cluster_evolution = [node]
    if alignments:
        node_first = node

        a0 = alignments[0]
        cluster_source = set(node_first["cluster"])
        index_source = a0["id2clusters_s"].index(cluster_source)
        # Following the alignments
        for a, h in zip(alignments, hierarchies[1:]):
            # Get the new node index and the corresponding cluster
            index_target = a["df_alignment"].loc[index_source].argmax()
            cluster_target = a["id2clusters_t"][index_target]
            # We stop if cluster_target and cluster_source have no element in common
            if not stop or len(cluster_source.intersection(cluster_target)):
                # Get the new node from the corresponding hierarchy
                with open(h, "rb") as openfile:
                    h_target = pickle.load(openfile)
                dict_clusters_t = {key: set(value) for key, value in nx.get_node_attributes(h_target, "cluster").items()
                                   }
                node_index_t = list(dict_clusters_t.keys())[list(dict_clusters_t.values()).index(cluster_target)]
                # Add the new node to the cluster evolution
                cluster_evolution.append(h_target.nodes[node_index_t])

                index_source = index_target
                cluster_source = cluster_target
            else:
                break

    return cluster_evolution


def past_evolution(node, hierarchies, alignments, stop=True):
    """
    Use the cluster_index in parameter to follows its evolution through the hierarchies by using the alignments but in
    the reversed order by selecting the most similar source
    -----
    :param node:            Node of hierarchies[-1]
    :param hierarchies:     List of n hierarchy files
    :param alignments:      List of n-1 associating dictionaries
    :param stop:            (Optional) If true we stop the evolution when no elements are shared by consecutive nodes
    :return:                Return a list of nodes containing the evolution in the reversed order of the cluster
                            with one node per period
    """
    cluster_evolution = [node]
    if alignments:
        node_last = node

        a = alignments[-1]
        cluster_target = set(node_last["cluster"])
        index_target = a["id2clusters_t"].index(cluster_target)  # Current target index
        # Following the alignments in reversed order
        for a, h in zip(reversed(alignments), reversed(hierarchies[:-1])):
            # Get the new node index and the corresponding cluster
            index_source = a["df_alignment"][index_target].argmax()
            cluster_source = a["id2clusters_s"][index_source]
            # We stop if cluster_target and cluster_source have no element in common
            if not stop or len(cluster_source.intersection(cluster_target)):
                # Get the new node from the source hierarchy
                with open(h, "rb") as openfile:
                    h_source = pickle.load(openfile)
                dict_clusters_s = {key: set(value) for key, value in nx.get_node_attributes(h_source, "cluster").items()
                                   }
                node_index_s = list(dict_clusters_s.keys())[list(dict_clusters_s.values()).index(cluster_source)]
                # Add the new node to the cluster evolution
                cluster_evolution.append(h_source.nodes[node_index_s])

                index_target = index_source
                cluster_target = cluster_source
            else:
                break

    return cluster_evolution


def format_label(cluster_labels):
    # Transform the label to a correct form
    n = len(cluster_labels)
    k = math.ceil(math.sqrt(n))
    c_label = ""
    index = 0
    while index + k < n:
        c_label += str(cluster_labels[index:index + k]) + "\n"
        index += k
    c_label += str(cluster_labels[index:])
    return c_label


class EvoMap(object):
    def __init__(self, df_timeline=None, diff=0, detected_nodes=None, query=None, name="evo_map"):
        self.__name = name          # A name given to the evo map
        self.__query = []           # A list of simple queries on which the evo map is based
        self.__df_evo_map = None    # A Pandas DataFrame containing the nodes of the evo map
        self.__detected_nodes = []  # A list of lists of nodes to highlight
        self.__nb_timelines = 0     # The number of timelines contained in the evo map
        self.__timelines = []       # The list of timelines that are lists of cluster hash
        if df_timeline is not None:
            self.__df_evo_map = df_timeline
            self.__df_evo_map["period"] = [i + diff for i in range(len(self.__df_evo_map))]
            self.__df_evo_map["appearance"] = [1 for _ in range(len(self.__df_evo_map))]
            self.__df_evo_map["index"] = [cluster_to_hash(c, i+diff) for i, c in
                                          enumerate(self.__df_evo_map["cluster"])]
            self.__df_evo_map["next"] = [{i} for i in list(self.__df_evo_map["index"])[1:]] + [set()]
            if detected_nodes:
                self.add_detected_nodes(detected_nodes)
            self.__nb_timelines += 1
            self.__timelines.append(list(self.__df_evo_map["index"]))
        if query:
            self.__query.append(query)

    # Name related methods #

    def name(self):
        return self.__name

    def rename(self, name):
        self.__name = name

    # Query related methods #

    def query(self):
        return self.__query

    def update_query(self, query):
        self.__query = query

    def add_timeline(self, df_timeline, diff=0, detected_node=None):
        # Init internal DataFrame if empty
        if self.__df_evo_map is None:
            self.__init__(df_timeline=df_timeline, diff=diff, detected_nodes=detected_node,
                          query=self.__query, name=self.__name)
        # Update known evo stops and add new ones
        else:
            id_old = ""
            timeline = []
            for j, c in enumerate(list(df_timeline["cluster"])):
                id_new = str(hash(frozenset(c))) + "_" + str(j + diff)
                timeline.append(id_new)
                if id_old:
                    # Add child index to the previous node
                    next_indices = list(self.__df_evo_map.loc[self.__df_evo_map["index"] == id_old, "next"])
                    self.__df_evo_map.loc[self.__df_evo_map["index"] == id_old, "next"] = [s | {id_new} for s in
                                                                                           next_indices]
                if id_new not in list(self.__df_evo_map["index"]):
                    new_node = dict(df_timeline.loc[j])
                    new_node["period"] = j + diff
                    new_node["appearance"] = 1
                    new_node["index"] = id_new
                    new_node["next"] = set()
                    self.__df_evo_map.loc[len(self.__df_evo_map)] = new_node
                else:
                    self.__df_evo_map.loc[self.__df_evo_map["index"] == id_new, "appearance"] += 1
                id_old = id_new
            # Increment the number of timelines
            self.__nb_timelines += 1
            self.__timelines.append(timeline)
            # Add a detected node
            if detected_node:
                self.add_detected_nodes(detected_node)

    def to_dataframe(self):
        """
        Convert the EvoMap object to a pandas DataFrame.
        -----
        "cluster"       is the content of a temporal node
        "d_birth"       is the smallest distance from which this cluster appears in the hierarchy
        "d_death"       is the greatest distance from which this cluster appears in the hierarchy
        "occurrence"    is the number of levels in which this cluster appears
        "period"        is the period index associated to this temporal node
        "appearance"    is the number of timelines in which this temporal node appears
        "index"         is key identifying the temporal node (hash of the cluster + period index)
        "next"          is a set of node indices following this temporal node in the evo map
        :return:        A Pandas DataFrame
        """
        return self.__df_evo_map

    def to_timelines(self):
        """
        Convert the EvoMap object to a list of timelines. Each timeline being a list of sets of terms.
        -----
        :return:    A list of sets of terms
        """
        # print(self.__timelines)
        return [[set(list(self.__df_evo_map.loc[self.__df_evo_map["index"] == h, "cluster"])[0]) for h in t]
                for t in self.__timelines]

    def add_detected_nodes(self, nodes, list_index=-1):
        if type(nodes) is list:
            self.__detected_nodes.append(nodes)
        else:
            if len(self.__detected_nodes) == 0:
                self.__detected_nodes = [[nodes]]
            else:
                self.__detected_nodes[list_index].append(nodes)

    def merge(self, m_map):
        df_m_map = m_map.to_dataframe()
        if self.__df_evo_map is None:
            self.__df_evo_map = df_m_map
        else:
            # Merge the node info
            for row in df_m_map.itertuples(index=False):
                # Update the appearance and the next nodes of a known evo stop
                if row.index in list(self.__df_evo_map["index"]):
                    self.__df_evo_map.loc[self.__df_evo_map["index"] == row.index, 'appearance'] = [
                        a + row.appearance for a in
                        self.__df_evo_map.loc[self.__df_evo_map["index"] == row.index, 'appearance']]
                    self.__df_evo_map.loc[self.__df_evo_map["index"] == row.index, 'next'] = [
                        next_indices | row.next for next_indices in
                        self.__df_evo_map.loc[self.__df_evo_map["index"] == row.index, 'next']]
                else:
                    # Or add the evo stop to the dataframe
                    self.__df_evo_map.loc[len(self.__df_evo_map)] = row
        # Update the number of timelines
        self.__nb_timelines += m_map.__nb_timelines
        # Update the timelines
        self.__timelines += m_map.__timelines
        # Merge the detected nodes
        self.__detected_nodes += m_map.__detected_nodes
        # Merge the queries
        self.__query += m_map.__query

    def to_graph(self, hierarchies, app_min=0, most_freq=None, save=None, period=None, graph_format='svg'):
        """

        :param hierarchies: A list of hierarchies covering consecutive periods of time
        :type hierarchies:  DiGraph hierarchies saved as pickle files
        :param app_min:     Fraction of appearance of a node among the timelines to de displayed
        :param most_freq:   Maximal number of words per topic to display (select the most frequent ones)
        :param save:        If a directory path is given, save the representation and
                            display it in an external window before returning it
        :param period:      Period parameter from the hierarchy names ("year", "publication_year", "month"...)
                            to add to the evo map as time axis captions
        :param graph_format:Format of the graphviz output. svg by default
        :return:            Return the representation as a graphviz directed graph
        """

        # Create the directed graph
        graph_evo_map = graphviz.Digraph(name=self.__name, format=graph_format)
        graph_evo_map.graph_attr['rankdir'] = 'LR'
        nb_periods = max(self.__df_evo_map['period'])
        next_period = nb_periods
        for i in range(nb_periods, -1, -1):

            # Create a subgraph for the current period
            with graph_evo_map.subgraph(name='sub_' + str(i)) as sub_graph_period_i:

                # Force the nodes to be at the same level
                sub_graph_period_i.graph_attr.update(rank='same')

                # Get all the node indices of the current period
                nodes_i = self.__df_evo_map[self.__df_evo_map['period'] == i].index

                # Add period information
                if period and len(nodes_i) > 0:
                    period_values = get_param(hierarchies[i], param=period)
                    if period_values:
                        period_label = "-".join([str(pv) for pv in period_values])
                        sub_graph_period_i.node(str(i), period_label, shape='box')
                        if i < nb_periods:
                            graph_evo_map.edge(str(i), str(next_period), color='black')
                    next_period = i

                # print(i, len(nodes_i))

                # Compute the label of these nodes (according to the most_freq parameter)
                if most_freq:

                    with open(hierarchies[i], "rb") as openfile:
                        hi = pickle.load(openfile)

                    df_frequencies_hi = pd.DataFrame(zip(hi.graph["labels"], hi.graph["frequencies"]),
                                                     columns=["label", "frequency"])
                    df_frequencies_hi = df_frequencies_hi.sort_values(by=['frequency'], ascending=False)

                    c_labels = []
                    for ci in self.__df_evo_map.loc[nodes_i, "cluster"]:

                        sorted_elements = df_frequencies_hi[df_frequencies_hi["label"].isin(list(ci))]
                        ci_label = list(sorted_elements[:most_freq]["label"])
                        ci_len = len(ci_label)
                        ci_label = format_label(ci_label)
                        # Add the real length of the cluster if we display only a subset of it
                        if ci_len < len(ci):
                            ci_label += "\n(" + str(len(ci)) + ")"
                        c_labels.append(ci_label)
                elif most_freq == 0:
                    nb_clusters_period_i = len(self.__df_evo_map.loc[nodes_i, "cluster"])
                    # c_labels = ["" for _ in range(nb_clusters_period_i)]
                    c_labels = ["c_" + str(i) + "_" + str(j) for j in range(nb_clusters_period_i)]
                else:
                    c_labels = [format_label(list(ci)) for ci in self.__df_evo_map[nodes_i]["cluster"]]

                # Add the labels to display to the evo map dataframe for the nodes of the current period
                self.__df_evo_map.loc[nodes_i, "label"] = c_labels

                # Process the nodes of the current period
                for node_i in self.__df_evo_map.iloc[nodes_i].itertuples(index=False):

                    # Add the node
                    if node_i.appearance / self.__nb_timelines >= app_min:

                        # Appearance in [t*nb_timelines;nb_timelines] mapped to [1;9]
                        # -> x in [a;b] equivalent to y = (x-a)/(b-a)*(d-c)+c in [c;d]
                        if self.__nb_timelines == 1:
                            col = 1
                        else:
                            col = int((node_i.appearance - app_min * self.__nb_timelines) / (self.__nb_timelines - 1) * 8 + 1)
                        fillcolor = "/greys9/" + str(col)
                        if col >= 6:
                            fontcolor = "white"
                        else:
                            fontcolor = "black"
                        sub_graph_period_i.node(node_i.index, node_i.label, style="filled",
                                                fillcolor=fillcolor,
                                                fontcolor=fontcolor)

                        # Add the edges
                        for next_tmp in node_i.next:
                            c = node_i.cluster
                            if list(self.__df_evo_map[self.__df_evo_map['index'] == next_tmp]['appearance'])[
                               0] / self.__nb_timelines >= app_min:
                                c_next = list(self.__df_evo_map[self.__df_evo_map['index'] == next_tmp]['cluster'])[0]
                                inter = set(c).intersection(c_next)
                                # Draw the arrow with 6 color shades according to the jaccard index between the nodes.
                                # If this index drops to 0, the edge is represented as a dashed arrow
                                if inter:
                                    w = float(len(inter)) / (len(c) + len(c_next) - len(inter))
                                    graph_evo_map.edge(node_i.index, next_tmp, color=str(int((10 * w) / 2) + 4),
                                                       colorscheme="greys9")
                                    # graph_evo_map.add_edge(id_new, id_old, weight=str(int(10 * w)))
                                else:
                                    graph_evo_map.edge(node_i.index, next_tmp, color="4", colorscheme="greys9",
                                                       style="dashed")

        # Highlight the detected nodes
        colors = []
        for li_nodes in self.__detected_nodes:
            # Get a distinct color for this list of detected nodes
            color = "#" + ''.join([random.choice('0123456789ABCDEF') for _ in range(6)])
            while color in colors:
                color = "#" + ''.join([random.choice('0123456789ABCDEF') for _ in range(6)])
            colors.append(color)
            # Add the color to the nodes
            for node in li_nodes:
                if list(self.__df_evo_map[self.__df_evo_map['index'] == node]['appearance'])[
                   0] / self.__nb_timelines >= app_min:
                    graph_evo_map.node(node, color=color, style="rounded,bold,filled", shape="box")

        # Save the evo map on a file
        if save:
            output = graph_evo_map.render(directory=save, view=True, cleanup=True)
            print("Graph saved in " + output)

        return graph_evo_map


def evolutionary_map_aux(simple_query, hierarchies, alignments, top_k=-1, wid_container=None):
    """
    Create a simple evo map representation of the best matching topics with the given bag of words and their evolutions
    through time.
    -----
    :param simple_query:    Bag of words to look at in the hierarchies
    :param hierarchies:     A list of hierarchies covering consecutive periods of time
    :type hierarchies:      DiGraph hierarchies saved as pickle files
    :param alignments:      A list of alignment dictionaries computed on the hierarchies
    :param top_k:           Number of top levels of the hierarchies to consider (related to the alignments)
    :param wid_container:   (Optional) Widget container where to display the process information.
                            If None the standard output is used
    :return:                Return an EvoMap object containing the timelines computed from the simple query
    """

    # Multi-timeline extractions #
    ##############################

    n = len(hierarchies)

    # Initialize the evo map
    evo_map_name = "evo_map." + "_".join(simple_query)
    if top_k > 0:
        evo_map_name += ".top_levels_" + str(top_k)
    evo_map = EvoMap(query=simple_query, name=evo_map_name)

    for i in log_progress(range(n), every=1, name="Processed timelines", wid_container=wid_container):

        # Init: find the best matching node in the ith hierarchy
        with open(hierarchies[i], "rb") as openfile:
            hi = pickle.load(openfile)
            # Reduce the graph to its top k levels
            if top_k > 0:
                hi = top_levels(hi, k=top_k)
        index_i, node_i = detect_cluster(hi, simple_query)
        # print(node_i)

        if index_i >= 0:

            # Compute past evolution
            cluster_past = past_evolution(node_i, hierarchies[:i+1], alignments[:i])
            # print(i, len(cluster_past))

            # Compute future evolution
            cluster_future = future_evolution(node_i, hierarchies[i:], alignments[i:])
            # print(i, len(cluster_future))

            # Merge the past with the future
            cluster_evolution = pd.DataFrame(list(reversed(cluster_past[1:])) + cluster_future)
            # print(i, cluster_evolution)

            diff = i - len(cluster_past) + 1
            evo_map.add_timeline(cluster_evolution, diff=diff, detected_node=cluster_to_hash(node_i["cluster"], i))
            # print(evo_map.to_dataframe())

    return evo_map


def evolutionary_map(query, hierarchies, alignments, period_min=0, period_max=None, name=None, wid_container=None):
    """
    Create a complex evolutionary map that is a representation of the best matching topics with the given bags of words
    and their evolutions through time.
    -----
    :param query:           A simple of complex query. Either a list of terms of a list of lists of terms.
    :param hierarchies:     A list of hierarchies covering consecutive periods of time
    :type hierarchies:      DiGraph hierarchies saved as pickle files
    :param alignments:      A list of alignment dictionaries computed on the hierarchies
    :param period_min:      Lower bound of the hierarchies (and alignments) to consider
    :param period_max:      Upper bound of the hierarchies (and alignments) to consider
    :param name:            Prefix of the name of the evo map
    :param wid_container:   (Optional) Widget container where to display the process information.
                            If None the standard output is used
    :return:                Return an EvoMap object containing the timelines computed from the given query
    """

    if not period_max:
        period_max = len(hierarchies)
    hi = hierarchies[period_min:period_max]
    al_tmp = alignments[period_min:period_max-1]
    al = []
    if al_tmp and isinstance(al_tmp[0], str):
        for a in al_tmp:
            al.append(load_from_pickle(a))
    else:
        al = al_tmp
    top_k = -1  # Number of top levels of the hierarchies to consider
    # Infer top_k from the first alignment
    if al:
        top_k = al[0]["top_k"]

    if query and isinstance(query[0], list):
        # Initialize the evo map
        evo_map_name = "evo_map"
        if name:
            evo_map_name = name
        if top_k > 0:
            evo_map_name += ".top_levels_" + str(top_k)
        # evo_map_name += "." + ".".join(["_".join(sq) for sq in query])
        evo_map_name += ".evo_map"
        evo_map = EvoMap(name=evo_map_name)

        n = len(query)
        for i in log_progress(range(n), every=1, name="Processed simple queries", wid_container=wid_container):
            simple_evo_map = evolutionary_map_aux(query[i], hi, al, top_k, wid_container)
            evo_map.merge(simple_evo_map)
            if not wid_container:
                str_query = str(query[i])
                if len(query[i]) > 10:
                    str_query = str(query[i][:10]) + " (" + str(len(query[i])) + ")"
                print(str(i+1) + "/" + str(n) + " simple queries processed: " + str_query)

        return evo_map
    else:
        return evolutionary_map_aux(query, hi, al, top_k=top_k, wid_container=wid_container)


def __timeline_similarity_aux(timeline1, timeline2, shift=0):
    """
    Compute the similarity between 2 timelines (list of clusters), the second being shifted according to the parameter.
    It uses the Jaccard similarity to compare the clusters.
    -----
    :param timeline1:   List of term clusters
    :param timeline2:   List of term clusters
    :param shift:       Shifting parameter of the second timeline
    :type:              Int
    :return:            Return a value of similarity as a float between 0 and 1
    """
    timeline1_shifted = timeline1.copy()
    timeline2_shifted = timeline2.copy()
    # Shift the timeline according to the shift parameter
    timeshift = [set() for _ in range(abs(shift))]
    if shift > 0:
        if shift >= len(timeline1_shifted):
            return 0
        else:
            timeline2_shifted = timeshift + timeline2_shifted
    else:
        if -1*shift >= len(timeline2_shifted):
            return 0
        else:
            timeline1_shifted = timeshift + timeline1_shifted
    # Complete timeline endings if necessary
    n1 = len(timeline1_shifted)
    n2 = len(timeline2_shifted)
    len_correction = [set() for _ in range(abs(n1-n2))]
    if n2 > n1:
        timeline1_shifted += len_correction
    else:
        timeline2_shifted += len_correction
    # print(timeline1, timeline1_shifted)
    # print(timeline2, timeline2_shifted)
    # Compute the similarity between topics of the timelines
    jaccard_sims = []
    for t1, t2 in zip(timeline1_shifted, timeline2_shifted):
        jaccard_sims.append(jaccard_similarity(t1, t2))
    return np.mean(jaccard_sims)


def timeline_similarity(timeline1, timeline2, shift=None):
    """
    Compute the similarity between 2 timelines (lists of clusters) by trying to find the shift that allows the best
    similarity (if not given in parameter). It uses the Jaccard index to compare the clusters.
    -----
    :param timeline1:   List of term clusters
    :param timeline2:   List of term clusters
    :param shift:       (Optional) Shifting parameter of the second timeline
    :type:              Int
    :return:            Return a value of similarity as a float between 0 and 1 and the associated shift
    """
    if shift is not None:
        return __timeline_similarity_aux(timeline1, timeline2, shift), shift
    else:
        best_similarity = 0
        best_shift = 0

        # Compute a set of timeline sim if one timeline is shorter than the other one of a unique sim if they have the
        # same length
        # print("Normal shifts")
        n1 = len(timeline1)
        n2 = len(timeline2)
        max_shift = n1 - n2
        step = 1
        if max_shift < 0:
            step = -1
        for shift in range(0, max_shift+step, step):
            similarity_tmp = __timeline_similarity_aux(timeline1, timeline2, shift)
            if similarity_tmp > best_similarity:
                best_similarity = similarity_tmp
                best_shift = shift
        # Try to shift further the timelines to see if the similarity increases
        # print("Extra shifts")
        max_len = max(n1, n2)
        min_len = min(n1, n2)
        shift = 1
        while shift < min_len:
            # The extra shift will force k=shift topics of be associated to the empty set hence with a jaccard sim of 0
            # This way the max possible sim is reduced so it may be useless to compute it
            if best_similarity >= 1 - shift/(max_len+shift):
                break
            else:
                similarity_tmp = __timeline_similarity_aux(timeline1, timeline2, -1*step*shift)
                if similarity_tmp > best_similarity:
                    best_similarity = similarity_tmp
                    best_shift = shift
                if max_shift != 0:
                    similarity_tmp = __timeline_similarity_aux(timeline1, timeline2, step*(abs(max_shift) + shift))
                    if similarity_tmp > best_similarity:
                        best_similarity = similarity_tmp
                        best_shift = shift
            shift += 1

        return best_similarity, best_shift


def evo_map_similarity(evo_map1, evo_map2):
    """
    Compute the similarity between 2 EvoMaps.
    -----
    It computes the timeline pairwise similarity matrix and take the average of the best values of the rows and the
    columns.
    :param evo_map1:   First EvoMap
    :param evo_map2:   Second EvoMap
    :return:           Return a value of similarity as a float between 0 and 1
    """
    timelines1 = evo_map1.to_timelines()
    timelines2 = evo_map2.to_timelines()
    similarity_matrix = np.array([[timeline_similarity(t1, t2)[0] for t2 in timelines2] for t1 in timelines1])
    sim_evo1_evo2 = similarity_matrix.max(0).mean()
    sim_evo2_evo1 = similarity_matrix.max(1).mean()
    return (sim_evo1_evo2 + sim_evo2_evo1) / 2
